//Controller.h
#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <iostream>
#include <string>
//ROS
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>
#include <robot_example_pkg/RobotStatus.h>
#include <robot_example_pkg/ChangeConstControl.h>

class Controller {
private:
   ros::NodeHandle nh_;
   ros::Publisher pub_, pub_status_;
   ros::Subscriber sub_;
   ros::ServiceServer service_;

public:
   Controller();
   void start();
   void odoCallback(geometry_msgs::Point pos_msg);
   bool changeContConst(robot_example_pkg::ChangeConstControl::Request  &req,
                        robot_example_pkg::ChangeConstControl::Response &res);
   
   double pos_x_, pos_y_, pos_z_, k_;
};

#endif /*CONTROLLER_H_*/
