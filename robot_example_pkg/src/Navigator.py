#!/usr/bin/env python
# Navigator.py
# ROS imports
import roslib; roslib.load_manifest('robot_example_pkg')
import rospy
from geometry_msgs.msg import Point

class Navigator(object):
    def __init__(self):
        self.pub_ = rospy.Publisher("/odometry", Point, queue_size = 1)
        return
    
    def start(self):
        #calculate position
        pos_msg = Point(1.0, 2.0, 5.0)
        loop_rate = rospy.Rate(1) # 1hz
        while not rospy.is_shutdown():
            self.pub_.publish(pos_msg);
            loop_rate.sleep()
        return

if __name__ == '__main__':
    rospy.init_node('navigator_py', log_level=rospy.INFO)
    rospy.loginfo("%s: starting navigator node", rospy.get_name())
    navigator = Navigator()
    navigator.start()
