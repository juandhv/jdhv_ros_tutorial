//Controller.cpp
#include "Controller.h"

Controller::Controller() {
	pub_ = nh_.advertise<geometry_msgs::Vector3>("/act_setpoint", 1);
	sub_ = nh_.subscribe("/odometry", 1, &Controller::odoCallback, this);
	pub_status_ = nh_.advertise<robot_example_pkg::RobotStatus>("/robot_status", 1);
	service_ = nh_.advertiseService("/controller/change_control_const", 
	                                 &Controller::changeContConst, this);
	k_ = 3.0;//controller constant, example
}


void Controller::start() {
	//calculate control action
	geometry_msgs::Vector3 force_msg;
	//status msg
	robot_example_pkg::RobotStatus status_msg;

	ros::Rate loop_rate(1);												
	while(ros::ok()) {
		force_msg.x = k_*pos_x_; force_msg.y = k_*pos_y_; force_msg.z = k_*pos_z_;//example
		pub_.publish(force_msg);

		status_msg.position.x = pos_x_; status_msg.position.y = pos_y_; status_msg.position.z = pos_z_;
		status_msg.force = force_msg;
		pub_status_.publish(status_msg);

		ros::spinOnce();
		loop_rate.sleep();
	}
}

void Controller::odoCallback(geometry_msgs::Point pos_msg) {
	pos_x_ = pos_msg.x;
	pos_y_ = pos_msg.y;
	pos_z_ = pos_msg.z;
}

bool Controller::changeContConst(robot_example_pkg::ChangeConstControl::Request  &req,
                                 robot_example_pkg::ChangeConstControl::Response &res){
	res.previous_k = k_;
	k_ = req.new_k;
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "controller_cpp");
	ROS_INFO("Starting controller node");
	Controller controller;
	controller.start();
	return (0);
}
