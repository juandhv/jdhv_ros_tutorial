#!/usr/bin/env python
# Controller.py
# ROS imports
import roslib; roslib.load_manifest('robot_example_pkg')
import rospy
from geometry_msgs.msg import Point, Vector3
from robot_example_pkg.msg import RobotStatus
from robot_example_pkg.srv import ChangeConstControl, ChangeConstControlResponse

class Controller(object):
    def __init__(self):
       self.pub_ = rospy.Publisher("/act_setpoint", Vector3, queue_size = 1)
       self.pub_status_ = rospy.Publisher("/robot_status", RobotStatus, queue_size = 1)
       self.sub_ = rospy.Subscriber("/odometry", Point, self.odoCallback, queue_size = 1)
       self.serv_ = rospy.Service('/controller/change_control_const_py', 
                                  ChangeConstControl, 
                                  self.changeContConst)
       self.pos_x_ = 0.0; self.pos_y_ = 0.0; self.pos_z_ = 0.0
       self.k_ = 3.0 #controller constant, example
       return
    
    def start(self): #calculate control action        
       force_msg = Vector3()
       status_msg = RobotStatus()
       loop_rate = rospy.Rate(1) # 1hz
       while not rospy.is_shutdown():
          force_msg.x = self.k_*self.pos_x_; force_msg.y = self.k_*self.pos_y_; 
          force_msg.z = self.k_*self.pos_z_;#example
          self.pub_.publish(force_msg)

          status_msg.position.x = self.pos_x_; 
          status_msg.position.y = self.pos_y_; 
          status_msg.position.z = self.pos_z_; 
          status_msg.force = force_msg
          self.pub_status_.publish(status_msg)
          loop_rate.sleep()
       return
        
    def odoCallback(self, pos_msg):
       self.pos_x_ = pos_msg.x; self.pos_y_ = pos_msg.y; self.pos_z_ = pos_msg.z
       return
    
    def changeContConst(self, req):
		previou_k = self.k_
		self.k_ = req.new_k
		return ChangeConstControlResponse(previou_k)

if __name__ == '__main__':
    rospy.init_node('controller_py', log_level=rospy.INFO)
    rospy.loginfo("%s: starting controller node", rospy.get_name())
    controller = Controller()
    controller.start()
