//Navigator.h
#ifndef NAVIGATOR_H_
#define NAVIGATOR_H_

#include <iostream>
#include <string>
//ROS
#include <ros/ros.h>
#include <geometry_msgs/Point.h>

class Navigator {
private:
   ros::NodeHandle nh_;
   ros::Publisher pub_;

public:
   Navigator();
   void start();
};

#endif /*NAVIGATOR_H_*/
