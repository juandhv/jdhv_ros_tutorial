//Navigator.cpp
#include "Navigator.h"

Navigator::Navigator() {
	pub_ = nh_.advertise<geometry_msgs::Point>("/odometry", 1);
}

void Navigator::start() {
	//calculate position
	geometry_msgs::Point pos_msg;
	pos_msg.x = 1.0; pos_msg.y = 2.0; pos_msg.z = 5.0;//example
	ros::Rate loop_rate(1);
	while(ros::ok()) {
		pub_.publish(pos_msg);
		loop_rate.sleep();
	}
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "navigator_cpp");
	ROS_INFO("Starting navigator node");
	Navigator navigator;
	navigator.start();
	return (0);
}
