#!/usr/bin/env python

# ROS imports
import roslib; roslib.load_manifest('hello_world_pkg')
import rospy

if __name__ == '__main__':
    rospy.init_node('hello_world_node_py', log_level=rospy.INFO)
    
    loop_rate = rospy.Rate(1) # 1hz
    while not rospy.is_shutdown():
        rospy.loginfo("%s: hello world (Python)", rospy.get_name())
        loop_rate.sleep()
