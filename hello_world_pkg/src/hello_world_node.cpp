#include <iostream>
#include <string>
#include <csignal>
//ROS
#include <ros/ros.h>

int main(int argc, char **argv) {
	ros::init(argc, argv, "hello_world_node_cpp");
	ros::NodeHandle node_handle;

	ros::Rate loop_rate(1);
	while(ros::ok()) {
		ROS_INFO("%s: hello world (C++)", ros::this_node::getName().c_str());
		loop_rate.sleep();
	}
	return (0);
}
